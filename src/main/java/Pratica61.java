
import java.util.*;
import utfpr.ct.dainf.if62c.pratica.*;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná DAINF - Departamento
 * Acadêmico de Informática
 *
 * Template de projeto de programa Java usando Maven.
 *
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica61 {

    public static void main(String[] args) {
        Time time1 = new Time();
        time1.addJogador("Goleiro", new Jogador(1, "Fulano"));
        time1.addJogador("Lateral", new Jogador(4, "Ciclano"));
        time1.addJogador("Atacante", new Jogador(10, "Beltrano"));

        Time time2 = new Time();
        time2.addJogador("Lateral", new Jogador(7, "José"));
        time2.addJogador("Goleiro", new Jogador(1, "João"));
        time2.addJogador("Atacante", new Jogador(15, "Mário"));

        Set<String> conjunto2 = time2.getJogadores().keySet();

        HashMap hash2 = time2.getJogadores();
        Set<Map.Entry<String, Jogador>> t2 = hash2.entrySet();

        HashMap hash1 = time1.getJogadores();
        Set<Map.Entry<String, Jogador>> t1 = hash1.entrySet();

        System.out.println("Posição\tTime 1\tTime 2");

        for (Map.Entry<String, Jogador> entry1 : t1) {
            System.out.println(entry1.getKey() + "\t" + entry1.getValue() + "\t" + hash2.get(entry1.getKey()));
        }
    }
}
